from jenkinsapi.jenkins import Jenkins
import sqlite3
from datetime import datetime
import time


class JenkinsInstance(object):

    def __init__(self, DB_PARAMS, JENKINS_PARAMS):
        self.DB_PARAMS = DB_PARAMS
        self.JENKINS_PARAMS = JENKINS_PARAMS
    
    def get_server_instance(self):
        try:
            server = Jenkins(self.JENKINS_PARAMS['url'], username = self.JENKINS_PARAMS['username'], password = self.JENKINS_PARAMS['password'])
        except:
            print ('can\'t connect to server')
            exit()
        return server

    def get_jobs(self):
        server = self.get_server_instance()
        job_description = server.get_jobs() #tuple containing multiple (job_name, job_instance) pairs
        nameStatusList = []
        for job_name, job_instance in job_description: 
            if job_instance.is_running():
                status = 'RUNNING'
            elif job_instance.get_last_build_or_none() == None :
                status = 'NOTBUILT'
            else:
                simple_job = server.get_job(job_instance.name)
                simple_build = simple_job.get_last_build()
                status = simple_build.get_status()
            time_checked = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
            
            nameStatusTuple = (job_instance.name, status, time_checked)
            nameStatusList.append(nameStatusTuple)
        return nameStatusList
    
    def save_job_status(self):      
        nameStatusList = self.get_jobs()
        print (len(nameStatusList),'jobs on Jenkins server')
        conn = sqlite3.connect(self.DB_PARAMS['db_name'])
        for nameStatus in nameStatusList:
            name, status, time_checked = nameStatus
            cur = conn.cursor()
            cur.execute("SELECT id FROM {} WHERE name=?".format(self.DB_PARAMS['table_name']), (name,))
            data=cur.fetchone()
            if data:
                #update
                updateTuple = (status, time_checked, name)
                cur.execute('UPDATE {} SET status=?, date_updated=? WHERE name=?'.format(self.DB_PARAMS['table_name']), updateTuple)
            else:
                #create
                createTuple = (name, status, time_checked)
                cur.execute('INSERT INTO {} (name, status, date_updated) VALUES (?,?,?)'.format(self.DB_PARAMS['table_name']), createTuple)
        
        # Save (commit) the changes
        conn.commit()
        
        # We can close the connection 
        conn.close()
    
        
        
DB_PARAMS = {
    'db_name': 'Jenkins.db',
    'table_name': 'jobs'
}

JENKINS_PARAMS = {
    'url': 'http://127.0.0.1:8080/',
    'username': 'josh',
    'password': 'accessme'
}

if __name__ == "__main__":
    newInstance = JenkinsInstance(DB_PARAMS, JENKINS_PARAMS)
    newInstance.save_job_status()
