# README #

jenkins.py is a python script that uses the Jenkins' API to get a list of jobs and their status from a given Jenkins instance.  The status for each job is stored in a sqlite database along with the time when it was checked.


### Requirements ###
* jenkinsapi==0.3.4

### Set up ###

* Clone the repository into a folder on your computer. 
* Install requirements. pip install requirements.txt
* Update the DB_PARAMS and JENKINS_PARAMS dictionary fields to configure the Database and Jenkins server respectively.
* Run the save_job_status() method of the JenkinsInstance object to query Jenkins server and populate the database.

### Database SQL Statement ###
	'''CREATE TABLE jobs
         (id INTEGER PRIMARY KEY AUTOINCREMENT,
         name VARCHAR(50) NOT NULL,
		 status VARCHAR(50) NOT NULL,
         date_updated VARCHAR(50) NOT NULL);'''


### Possible Job Status ###
	
* SUCCESS
* FAILED
* NOTBUILT
* RUNNING

### Assumptions ###
	
sqlite doesn't store datetime objects, thus dates were saved as strings

The jobs table will have unique entries for the name field. 
Thus when the script is run, if the jobs table already has the job status stored from a previous insertion command, then the job record is updated (status, date_updated). 
Else, a new record is created in the jobs table to store the job status.
